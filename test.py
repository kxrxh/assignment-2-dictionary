import subprocess

inputs = ["honda", "mazda", "toyota", "nissan", "subaru", "vw", "bmw_",
               "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest"]
expected = ["civic", "3", "corolla", "leaf", "impreza", "", "", ""]


expected_err = ["", "", "", "", "", "Unable to find key",
                   "Unable to find key", "Invalid ket value"]
num_failures = 0

print("🚀Starting🚀")

for i in range(len(inputs)):
    process = subprocess.Popen(
        ["./build/main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    if stdout == expected[i] and stderr == expected_err[i]:
        print(f"✅ Test {i} passed")
    else:
        num_failures += 1
        print(f"❌ Test {i} failed")
        print(f"\t🔍 Input: {inputs[i]}")
        print(f"\t✨ Awaited output: {expected[i]}")
        print(f"\t📤 Stdout: {stdout.strip()}")
        print(f"\t❗ Awaited error: {expected_err[i]}")
        print(f"\t🚨 Stderr: {stderr.strip()}")
        print(f"\t🔢 Exit code: {process.returncode}")
print("➖➖➖➖➖➖➖➖➖➖➖➖")
if num_failures == 0:
    print("✅✅All tests passed✅✅")
else:
    print("❌❌{num_failures} tests failed❌❌")