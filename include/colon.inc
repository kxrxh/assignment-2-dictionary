%define colon_ptr 0

%macro colon 2
    %ifid %2
        %2: dq colon_ptr
        %define colon_ptr %2
    %else
        %error "Second argument isn't an id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "First argument must be a string"
    %endif
%endmacro
