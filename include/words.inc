%include "./include/colon.inc"

section .data
    colon "honda", key_honda
    db "civic", 0

    colon "mazda", key_mazda
    db "3", 0

    colon "toyota", key_toyota
    db "corolla", 0

    colon "nissan", key_nissan
    db "leaf", 0

    colon "subaru", key_subaru
    db "impreza", 0

    colon "ford", key_ford
    db "focus", 0

    colon "bmw", key_bmw
    db "m3", 0
