%define SYSTEM_EXIT  60
%define SYSTEM_WRITE 1
%define SYSTEM_READ  0
%define STDOUT       1
%define STDERR       2


%macro CHECK_WHITESPACE_AND_JUMP 2
    cmp %1, ' '  ; Check for space
    je  %2
    cmp %1, `\t` ; Check for tab
    je  %2
    cmp %1, `\n` ; Check for newline
    je  %2
%endmacro

section .text

global  exit
global  exit0
global  string_length
global  string_equals
global  string_copy

global  print_error
global  print_string
global  print_newline
global  print_char
global  print_int
global  print_uint

global  read_char
global  read_word
global  parse_uint
global  parse_int



exit0:
    xor rdi, rdi
; Принимает код возврата и завершает текущий процесс
; args: rdi
exit: 
    mov rax, SYSTEM_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; args: rdi
; out: rax
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0 ; if *(array_ptr + index) == 0
        je  .end
        inc rax                 ; index++
        jmp .loop
    .end:
        ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    ; go down to print_char

; Принимает код символа и выводит его в stdout
; args: rdi
print_char:
    push rdi               ; save rdi to stack
    mov  rax, SYSTEM_WRITE
    mov  rdi, STDOUT       ; stdout
    mov  rsi, rsp          ; set rsi = pointer to rdi
    mov  rdx, 1
    syscall
    pop  rdi               ; restore rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; args: rdi
print_string:
    push rdi
    call string_length     ; now rax = string_length
    mov  rdi, STDOUT       ; stdout
    pop  rsi               ; set rsi = rdi
    mov  rdx, rax          ; set rdx = rax (string_length)
    mov  rax, SYSTEM_WRITE
    syscall
    ret

print_error:
    push rdi
    call string_length     ; now rax = string_length
    mov  rdi, STDERR       ; stdout
    pop  rsi               ; set rsi = rdi
    mov  rdx, rax          ; set rdx = rax (string_length)
    mov  rax, SYSTEM_WRITE
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; args: rdi
print_int:
    test rdi, rdi   ; updating SF flag
    jns  print_uint ; if number >= 0 then print_uint
    ; else print '-'
    push rdi
    mov  rdi, '-'   ; pass '-' as argument
    call print_char
    pop  rdi
    neg  rdi        ; represent rdi as unsigned number
    ; go down to print_uint


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; args: rdi
print_uint:
    push r12

    mov rax,        rdi ; rax = rdi (number)
    mov rcx,        10  ; Divisor
    mov r12,        1   ; Counter (starts with 1, because we have \0)
    dec rsp
    mov byte [rsp], 0   ; Push \0 to stack
    .loop:
        xor  rdx,   rdx ; rdx = 0
        div  rcx
        ; Reminder in rdx now (rax % r8 = rdx)
        add  dl,    '0' ; Adding ASCII code of 0 to rdx
        dec  rsp        ; Decrement stack
        mov  [rsp], dl  ; Push reminder to stack
        inc  r12        ; Increment counter
        test rax,   rax ; If number is 0
        jne  .loop
    .end:
        mov  rdi, rsp     ; pass rsp as argument to function
        call print_string
        add  rsp, r12     ; restore rsp
        pop  r12
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; args: rdi, rsi
; out: rax
string_equals:
    xor rcx, rcx ; use rcx as index for strings. rcx = 0
    .loop:
        movzx rax, byte [rdi + rcx] ; rax = *(array_ptr + index)
        cmp   al,  byte [rsi + rcx] ; if *(array_ptr + index) == *(array_ptr + index)
        jne   .return_false         ; if *(array_ptr + index) != *(array_ptr + index) then return 0
        test  al,  al               ; if *(array_ptr + index) == 0
        jz    .return_true
        inc   rcx                   ; index++
        jmp   .loop
    .return_true:
        mov rax, 1
        ret
    .return_false:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; out: rax
read_char:
    xor rdi, rdi         ; mov rdi, 0 stdin
    mov rax, SYSTEM_READ

    dec rsp      ; buffering space for char on stack
    mov rsi, rsp ; set rsi = pointer to rsp

    mov rdx, 1
    syscall

    test rax, rax
    je   .end

    movzx rax, byte [rsp]
.end:
    inc rsp ; restore rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; args: rdi, rsi
; out: rax, rdx
read_word:
    ; using callee-saved registers
    push r12
    push r13
    push r14

    mov r12, rdi ; pointer to buffer
    mov r13, rsi ; buffer size
    xor r14, r14 ; word length

.loop_skip_spaces:
    call read_char

    CHECK_WHITESPACE_AND_JUMP al, .loop_skip_spaces

    .word_loop:
        test al, al          ; if *(array_ptr + index) == 0
        jz   .read_word_done
        
        cmp r14, r13            ; if word length == buffer size -> word is too long
        je  .read_word_too_long

        CHECK_WHITESPACE_AND_JUMP al, .read_word_done

        mov [r12 + r14], al ; write char to buffer
        inc r14
        
        call read_char ; read next char

        jmp .word_loop

    .read_word_too_long:
        xor rax, rax ; rax = 0 if word is too long
        jmp .end

    .read_word_done:
        cmp r14, r13
        je  .read_word_too_long

        mov rax,            r12 ;
        mov byte [r12+r14], 0
        mov rdx,            r14 ; set length
    .end:
        pop r14
        pop r13
        pop r12
        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; args: rdi
; out: rax, rdx
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        movzx rcx, byte [rdi + rdx] ; cl = *(array_ptr + index)

        test cl, cl ; if *(array_ptr + index) == 0 then break
        jz   .end

        sub cl, '0' ; cl = cl - '0'

        cmp cl, 0 ; if cl < 0 then break
        jl  .end

        cmp cl, 9 ; if cl > 9 then break
        jg  .end

        imul rax, 10  ; rax = rax * 10
        add  rax, rcx ; rax = rax + cl
        inc  rdx

        jmp .loop
    .end:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; args: rdi
; out: rax, rdx
parse_int:
    push r12

    xor rdx, rdx
    xor r12, r12

    movzx rcx, byte [rdi] ; cl = *(array_ptr)

    test cl, cl ; if *(array_ptr) == 0 then break
    jz   .end

    cmp cl, '-'
    je  .minus

    cmp cl, '+'
    je  .plus

    jmp .parse

    .minus:
        inc r12
    .plus:
        inc rdi

    .parse:
        call parse_uint
        test r12, r12
        jz   .end
        add  rdx, r12
        neg  rax        ; rax = -rax        
    .end:
        pop r12
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; args: rdi: string, rsi: buffer, rdx: buffer size
; out: rax
string_copy:
    xor rcx, rcx
    .loop:
        movzx rax,             byte [rdi + rcx] ; rax = *(array_ptr + index)
        mov   byte[rsi + rcx], al               ; *(buffer_ptr + index) = *(array_ptr + index)
        test  al,              al               ; if *(array_ptr + index) == 0
        je    .return_length

        inc rcx ; length++
        
        cmp rcx, rdx
        jb  .loop    ; if index < buffer_size then loop
        
        mov rax, 0 ; else error -> rax = 0
        jmp .end
    .return_length:
        mov rax, rcx
    .end:
        ret
