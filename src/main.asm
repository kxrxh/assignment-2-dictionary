%include "include/lib.inc"
%include "include/words.inc"
%include "include/dict.inc"
%define BUF_LEN 255
global  _start

section .bss
    buffer: resb BUF_LEN ; Buffer for input word, allocating 255 bytes

section .text

section .rodata:
    find_err_msg:  db "Unable to find key", 0
    input_err_msg: db "Invalid ket value", 0

_start:
    mov rdi, buffer ; RDI - pointer to the buffer
    mov rsi, BUF_LEN    ; RSI - maximum word length

    call read_word
    test rax, rax
    jz   .input_error ; If the result is zero, jump to input error handling

    mov rdi, buffer
    mov rsi, colon_ptr
    
    call find_word   ; Search the entered word in the dictionary
    test rax, rax    ; Check the result of the search
    jz   .find_error ; If the result is zero, jump to search error handling

    lea  rdi, [rax + 8] ; RDI - pointer to the start of the word description (offset by 8 bytes)
    call string_length  ; Call the function to determine the length of the string

    add rdi, rax ; Increase RDI by the length of the found string
    inc rdi      ; Add 1 for the null-terminating character

    call print_string ; Print the found string
    call exit0        ; Exit the program

.input_error:
    mov rdi, input_err_msg ; RDI - pointer to the input error message
    jmp .err_call          ; Jump to error handling

.find_error:
    mov rdi, find_err_msg ; RDI - pointer to the search error message

.err_call:
    call print_error ; Print the error message
    mov  rdi, 1      ; Set the error code in RDI
    call exit        ; Exit the program
