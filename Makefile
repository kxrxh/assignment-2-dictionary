NASM = nasm
NASM_FLAGS = -f elf64
LD = ld
PYTHON = python
SRC_DIR = src
INC_DIR = include
BUILD_DIR = build
SRCS := $(wildcard $(SRC_DIR)/*.asm)
OBJS := $(patsubst $(SRC_DIR)/%.asm,$(BUILD_DIR)/%.o,$(SRCS))
TARGET = $(BUILD_DIR)/main

# Specify dependencies for each .asm file
$(BUILD_DIR)/main.o: $(SRC_DIR)/main.asm $(INC_DIR)/lib.inc $(INC_DIR)/words.inc $(INC_DIR)/dict.inc
	@mkdir -p $(dir $@)
	$(NASM) $(NASM_FLAGS) $< -o $@

$(BUILD_DIR)/dict.o: $(SRC_DIR)/dict.asm $(INC_DIR)/lib.inc
	@mkdir -p $(dir $@)
	$(NASM) $(NASM_FLAGS) $< -o $@

$(BUILD_DIR)/lib.o: $(SRC_DIR)/lib.asm
	@mkdir -p $(dir $@)
	$(NASM) $(NASM_FLAGS) $< -o $@

# Link the objects to create the target
$(TARGET): $(OBJS)
	$(LD) $(LD_FLAGS) $^ -o $@

# Clean build directory
clean:
	rm -rf $(BUILD_DIR)

# Run the target
run: $(TARGET)
	./$(TARGET)

# Add a 'test' target for running tests if needed
test: $(TARGET)
	$(PYTHON) test.py
	$(MAKE) clean
